import requests
import bs4
from bs4 import Comment
from bs4 import NavigableString
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException 
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import ElementNotVisibleException
import csv
import time
import os
import re
import gspread
import sys
import datetime

'''

This includes a requirement to download PhantomJS webdriver to run or you can use Firefox - Comment out as needed below in class

'''

def find_data_file(filename):
	if getattr(sys, 'frozen', False):
		# The application is frozen
		datadir = os.path.dirname(sys.executable)
	else:
		# The application is not frozen
		# Change this bit to match where you store your data files:
		datadir = os.path.dirname(os.path.abspath(__file__)) #This grabs the directory of the script for finding the needed things more easily
	return os.path.join(datadir, filename)

def wait_for_pop(condition_function, string):
	start_time = time.time()
	while time.time() < start_time + 60:
		try: 
			condition_function(string)
			return True
		except NoSuchElementException:
			time.sleep(0.1)
	raise Exception('Timeout waiting for {}'.format(condition_function.__name__))

def wait_for(condition_function): #The wait function used in the page load wait class
	start_time = time.time()
	while time.time() < start_time + 180:
		if condition_function():
			return True
		else:
			time.sleep(0.1)
	raise Exception('Timeout waiting for {}'.format(condition_function.__name__))
		
class wait_for_page_load(object): #Needed to add sufficient waits based on page element staleness - or if an element can no longer be found

	def __init__(self, driver): #Designate browser
		self.driver = driver

	def __enter__(self): #Find the element
		self.old_page = self.driver.find_element_by_tag_name('html')

	def page_has_loaded(self): #Check for staleness
		new_page = self.driver.find_element_by_tag_name('html')
		return new_page.id != self.old_page.id

	def __exit__(self, *_): #Wait and then exit class with
		wait_for(self.page_has_loaded)
		
	'''
	You use it like this -
	with wait_for_page_load(driver):
		driver.find_element_by_link_text('my link').click()
	'''

class userData(object):

	def __init__(self):
		#self.driver = webdriver.Firefox()
		flag = False
		counterKeep = 0
		while flag == False:
			try: 
				self.driver = webdriver.PhantomJS(executable_path=find_data_file('phantomjs.exe'))
				#Phantom is nearly three times faster than a visual browser
				flag = True
			except WebDriverException:
				counterKeep += 1
				flag = False
			if counterKeep > 100: raise Exception('Unable to start driver!')
		self.driver.set_window_size(1120, 550)

	def getPage(self, url='roosterteeth.com/burnie'):
		if 'http://' not in url.lower():
			url = 'http://' + url
		self.driver.get(url)
		print('Got page!')
		return url

	def check_by_css_selector(self, css_string):
			try:self.driver.find_element_by_css_selector(css_string)
			except NoSuchElementException:
				return False
			return True	

	def check_by_name(self, name):
		try: self.driver.find_element_by_name(name)
		except NoSuchElementException:
			return False
		return True	
		
	def check_by_xpath(self, path):
		try:self.driver.find_element_by_xpath(path)
		except NoSuchElementException:
			return False
		return True

	def check_by_id(self, name):
		try:self.driver.find_element_by_id(name)
		except NoSuchElementException:
			return False
		return True

	def check_by_link_text(self, text):
		try:self.driver.find_element_by_link_text(text)
		except NoSuchElementException:
			return False
		return True

	def currentdatetime(self):
		return datetime.datetime.now()

	def removeCommonTags(self, stringVal):
		# Remove misc tags
		tempString = stringVal.replace("\n"," ")
		tempString = tempString.replace("\t"," ")
		tempString = tempString.replace("\xa0"," ")
		tempString = tempString.replace("\u25ba"," ")
		tempString = tempString.replace('[if !supportLists]'," ")
		tempString = tempString.replace('[endif]'," ")

		return tempString

	def internet_on(self, url='http://www.google.com/', timeout=5):
		try:
			_ = requests.get(url, timeout=timeout)
			return True
		except requests.ConnectionError:
			print('No internet connection available')
		return False

	def wait_for_internet(self):
		count = 0
		while not self.internet_on():
			count += 1
			if count > 120:
				print('Timed out! Request took longer than ten minutes!')
				exit()

	def shutDown(self):
		self.driver.quit()

	def bio(self, url='roosterteeth.com/burnie'):
		self.username = ''
		self.karma = ''
		self.signUpDate = ''
		self.memberSince = ''
		self.lastSignIn = ''
		self.name = ''
		self.age = ''
		self.location = ''
		self.gender = ''
		self.occupation = ''
		self.description = ''
		self.birthdate = ''
		self.sponsorStatus = False
		self.nextStaff = False
		self.nextStone = False
		self.milestones = []
		self.staffAwards = []
		self.gamertag = ''
		self.groups = []
		self.wishlist = []
		self.games = []
		self.videos = []
		self.events = []
		self.dataDict = {}

		self.wait_for_internet()

		url = self.getPage(url)
		try: 
			if self.driver.find_element_by_xpath('//*[@id="pageContent"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[3]/table[1]/tbody/tr/td/table/tbody/tr/td[1]/table/tbody/tr/td[1]').text.lower() != url.lower().split('/')[3]:
				raise Exception('Incorrect URL for Roosterteeth User Page! Format is: ' + 'http://roosterteeth.com/username')
		except NoSuchElementException:
			raise Exception('Incorrect URL for Roosterteeth User Page! Format is: ' + 'http://roosterteeth.com/username')

		self.wait_for_internet()
		while not wait_for_pop(self.driver.find_element_by_id, 'profileAjaxContent'):
			self.driver.find_element_by_id('profileNavprofile').click()

		self.username = self.driver.find_element_by_xpath('//*[@id="pageContent"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[3]/table[1]/tbody/tr/td/table/tbody/tr/td[1]/table/tbody/tr/td[1]').text
		print(self.username)
		self.dataDict[self.username] = []

		self.karma = self.driver.find_element_by_xpath('//*[@id="karmaProgressBarBar"]').text
		print(self.karma)

		self.signUpDate = self.driver.find_element_by_xpath('//*[@id="pageContent"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[1]/table/tbody/tr[5]/td/table/tbody/tr[1]/td/table/tbody/tr/td[2]').text
		tempDate = self.signUpDate.split('(', 1)[1]
		tempDate = tempDate.replace(' ', '')
		tempDate = tempDate.replace('(', '')
		tempDate = tempDate.replace(')', '')
		tempDate = tempDate.replace('/', ' ')
		if len(tempDate) != 8:
			tempDate = '0' + tempDate #Make sure that the characters expected are there should months not be double digit already
		self.signUpDate = time.strptime(tempDate, "%m %d %y")
		self.signUpDate = datetime.datetime(*self.signUpDate[:6])
		print(self.signUpDate)

		self.memberSince = self.driver.find_element_by_xpath('//*[@id="pageContent"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[1]/table/tbody/tr[5]/td/table/tbody/tr[1]/td/table/tbody/tr/td[2]/span').text
		print(self.memberSince)

		if self.driver.find_element_by_xpath('//*[@id="pageContent"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[1]/table/tbody/tr[5]/td/table/tbody/tr[2]/td[2]/span'):
			self.lastSignIn = self.driver.find_element_by_xpath('//*[@id="pageContent"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[1]/table/tbody/tr[5]/td/table/tbody/tr[2]/td[2]/span').text
		print(self.lastSignIn)

		profileContentMain = self.driver.find_element_by_xpath('//*[@id="profileAjaxContent"]/table/tbody')

		counter = 0
		flag = False
		for row in profileContentMain.find_elements_by_tag_name('tr'):
			if not flag:
				try:
					childTemp = row.find_element_by_css_selector("div[class='titleLine']")
					if childTemp.text.lower() == 'awards':
						flag = True
						startTemp = counter
						continue
				except NoSuchElementException:
					counter += 1
					continue
			else:
				try:
					childTemp = row.find_element_by_css_selector("div[class='titleLine']")
					if childTemp.text.lower() == (str(self.username.lower()) + "'s..."):
						flag = False
						stopTemp = counter
						continue
				except NoSuchElementException:
					counter += 1
					continue
			counter += 1

		self.infoRows = profileContentMain.find_elements_by_tag_name('tr')
		for row in self.infoRows:

			if self.infoRows.index(row) >= startTemp and self.infoRows.index(row) <= stopTemp:

				if 'name' in row.find_element_by_xpath('.//td[1]').text.lower():
					self.name = row.find_element_by_xpath('.//td[2]').text
					print(self.name)
				if 'birthday' in row.find_element_by_xpath('.//td[1]').text.lower():
					self.birthdate = row.find_element_by_xpath('.//td[2]').text
					#Convert this to datetime object
					tempDate = self.birthdate.replace('th', '')
					tempDate = self.birthdate.replace('rd', '')
					tempDate = tempDate.replace(',', '')
					self.birthdate = time.strptime(tempDate,"%B %d %Y")
					self.birthdate = datetime.datetime(*self.birthdate[:6])
					print(self.birthdate)
				if 'occupation' in row.find_element_by_xpath('.//td[1]').text.lower():
					self.occupation = row.find_element_by_xpath('.//td[2]').text
					print(self.occupation)
				if 'gamertag' in row.find_element_by_xpath('.//td[1]').text.lower():
					self.gamertag = row.find_element_by_xpath('.//td[2]').text
					print(self.gamertag)
				if 'staff awards' in row.find_element_by_xpath('.//td[1]').text.lower():
					print("Staff Awards Found!")
					self.nextStaff = True
				if 'milestones' in row.find_element_by_xpath('.//td[1]').text.lower():
					print('Milestones Found!')
					self.nextStone = True
				if self.nextStaff:
					self.nextStaff = False
					self.staffAwards = row.find_elements_by_tag_name('div')
					awardList = []
					for div in self.staffAwards:
						awardList.append(div.find_element_by_css_selector('img').get_attribute('title'))
					self.staffAwards = [x for x in awardList if x]
					print(self.staffAwards)
					#This is an html object that contains the divs which house the awards
				if self.nextStone:
					self.nextStone = False
					self.milestones = row.find_elements_by_tag_name('div')
					awardList = []
					for div in self.milestones:
						stringTemp = div.find_element_by_css_selector('img').get_attribute('onmouseover')
						if stringTemp:
							stringTemp = stringTemp.split("<br />")[1]
							stringTemp = stringTemp[:-2]
							awardList.append(stringTemp)
					self.milestones = [x for x in awardList if x]
					print(self.milestones)
					#This is an html object that contains the divs which house the awards

			elif self.infoRows.index(row) == 0:

				self.descBody = row.find_element_by_xpath('.//td')
				child = self.descBody.find_element_by_tag_name('span')
				self.description = self.descBody.text.replace(child.text, '')
				print(self.description)
				self.mixedDemographicInfo = child.text
				print(child.text)
				self.mixedDemographicInfoList = self.mixedDemographicInfo.split(' ')
				self.age = self.mixedDemographicInfoList[0]
				print(self.age)
				self.gender = self.mixedDemographicInfoList[2]
				print(self.gender)
				if len(self.mixedDemographicInfoList) > 3:
					self.location = self.mixedDemographicInfoList[4] + ' ' + self.mixedDemographicInfoList[5]
				else:
					self.location = False
				print(self.location)

			elif self.infoRows.index(row) > stopTemp:

				try:
					row.find_element_by_css_selector("a[href='/groups/']")
					self.groups = []
					try: 
						row.find_element_by_xpath('.//td[2]/a').click()
						print('Clicked Button Groups!')
					except NoSuchElementException:
						pass

					listGroups = self.driver.find_elements_by_id('likeItemi')
					for div in listGroups:
						self.groups.append(div.find_element_by_css_selector("div[class='light likeLight']").text)
					print(self.groups)
				except NoSuchElementException:
					pass

				try:
					if 'wishlist.php' in row.find_element_by_css_selector("a[class='small light']").get_attribute('href'):
						self.wishlist = []
						listwishes = []
						try: 
							row.find_element_by_xpath('.//td[2]/a').click()
							listwishes = self.driver.find_element_by_id('userLikesMorewishlist')
							listwishes = listwishes.find_elements_by_css_selector("div[class='light likeLight']")
							print('Clicked Button Wish!')
						except NoSuchElementException:
							pass
							
						listwishes += self.driver.find_element_by_id('userLikeswishlist').find_elements_by_css_selector("div[class='light likeLight']")
						for div in listwishes:
							self.wishlist.append(div.text)
						print(self.wishlist)
				except NoSuchElementException:
					pass

				try:
					row.find_element_by_css_selector("a[href='/games/']")
					self.games = []
					listgames = []
					try: 
						row.find_element_by_xpath('.//td[2]/a').click()
						listgames = self.driver.find_element_by_id('userLikesMoregames')
						listgames = listgames.find_elements_by_css_selector("div[class='light likeLight']")
						print('Clicked Button Games!')
					except NoSuchElementException:
						pass

					listgames += self.driver.find_element_by_id('userLikesgames').find_elements_by_css_selector("div[class='light likeLight']")
					for div in listgames:
						self.games.append(div.text)
					print(self.games)
				except NoSuchElementException:
					pass

				try:
					row.find_element_by_css_selector("a[href='/archive/']")
					self.videos = []
					listvideos = []
					try: 
						row.find_element_by_xpath('.//td[2]/a').click()
						listvideos = self.driver.find_element_by_id('userLikesMorevideos')
						listvideos = listvideos.find_elements_by_css_selector("div[class='light likeLight']")
						print('Clicked Button Videos!')
					except NoSuchElementException:
						pass

					listvideos += self.driver.find_element_by_id('userLikesvideos').find_elements_by_css_selector("div[class='light likeLight']")
					for div in listvideos:
						self.videos.append(div.text)
					print(self.videos)
				except NoSuchElementException:
					pass

				try:
					row.find_element_by_css_selector("a[href='/events/']")
					self.events = []
					listevents = []
					try: 
						row.find_element_by_xpath('.//td[2]/a').click()
						listevents = self.driver.find_element_by_id('userLikesMoreevents')
						listevents = listevents.find_elements_by_css_selector("div[class='light likeLight']")
						print('Clicked Button Events!')
					except NoSuchElementException:
						pass

					listevents += self.driver.find_element_by_id('userLikesevents').find_elements_by_css_selector("div[class='light likeLight']")
					for div in listevents:
						self.events.append(div.text)
					print(self.events)
				except NoSuchElementException:
					pass

		if self.check_by_css_selector("img[original-title='Sponsor']") or self.check_by_css_selector("img[title='Sponsor']"):
			self.sponsorStatus = 'SPONSOR'

		elif self.check_by_css_selector("a[class='modTitleLinkBig']"):
			self.sponsorStatus = self.driver.find_element_by_class_name('modTitleLinkBig').text

		else:
			self.sponsorStatus = False

		print(self.sponsorStatus)

		self.dataDict[self.username] = [self.name, self.age, self.location, self.gender, self.removeCommonTags(self.occupation), self.karma, self.removeCommonTags(self.description), self.birthdate,\
			self.signUpDate, self.lastSignIn, self.memberSince, self.sponsorStatus, self.milestones, self.staffAwards, self.gamertag, \
			self.groups, self.wishlist, self.games, self.videos, self.events, self.currentdatetime()]

		return self.dataDict

	def commentStream(self, url='roosterteeth.com/burnie'):
		'''

		Pull the most recent comments (30 of them as you could if you were an anonymous reader)

		'''
		self.wait_for_internet()
		url = self.getPage(url)
		try: 
			if self.driver.find_element_by_xpath('//*[@id="pageContent"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[3]/table[1]/tbody/tr/td/table/tbody/tr/td[1]/table/tbody/tr/td[1]').text.lower() != url.lower().split('/')[3]:
				raise Exception('Incorrect URL for Roosterteeth User Page! Format is: ' + 'http://roosterteeth.com/username')
		except NoSuchElementException:
			raise Exception('Incorrect URL for Roosterteeth User Page! Format is: ' + 'http://roosterteeth.com/username')


		self.dataDict = {}
		self.commentDict = {}
		seeMores = []
		seeMoresTemp = []

		self.wait_for_internet()
		with wait_for_page_load(self.driver):
			self.driver.find_element_by_id('profileNavcomments').click()

		self.commentZone = self.driver.find_element_by_id('commentsArea')
		try: self.commentZone.find_elements_by_css_selector("div")
		except NoSuchElementException:
			return None

		while self.check_by_xpath("//a[contains(@onclick,'loadMoreReplies') and .//text()='See More']"):
			self.commentZone = self.driver.find_element_by_id('commentsArea')
			try: self.commentZone.find_element_by_xpath("//a[contains(@onclick,'loadMoreReplies') and .//text()='See More']").click()
			except NoSuchElementException: pass

		while self.check_by_link_text('See More'):
			print("I exist said the see more!")
			self.commentZone = self.driver.find_element_by_id('commentsArea')
			try: 
				seeMores = self.commentZone.find_elements_by_link_text('See More')
				print('Found the link to see more!')
				break
			except NoSuchElementException: 
				break

		for x in seeMores:
			if x.get_attribute('class') == 'fastTouch' and x.get_attribute('href') == 'javascript:;' and x.text == 'See More' and 'loadMoreReplies' not in x.get_attribute('onclick'):
				seeMoresTemp.append(x.get_attribute('onclick'))

		for x in seeMoresTemp:
			self.commentZone = self.driver.find_element_by_id('commentsArea')
			s = x.split('"')[1]
			print(s)
			try: 
				tempDiv = self.commentZone.find_element_by_id(s)
				tempDiv.find_element_by_css_selector("a[class='fastTouch']").click()
				print('Clicked to see more!')
			except ElementNotVisibleException: continue

		self.commentStream = self.commentZone.find_elements_by_css_selector("td[class='streamMessage']")
		for comment in self.commentStream:

			userLinks = []
			replyUserLinks = []
			tempLinks = []
			tempReplyImgs = []
			replies = []
			repliesDict = {}

			userTemp = comment.find_element_by_css_selector("a[class='light']").text
			postedDateTemp = comment.find_element_by_css_selector("span[class='updateWhen']").text
			commentTemp = comment.text
			removeTemp = comment.find_element_by_xpath('.//table[2]').text
			commentTemp = commentTemp.replace(removeTemp,'')
			removeTemp = comment.find_element_by_xpath('.//table[1]').text
			commentTemp = commentTemp.replace(removeTemp,'')
			excludeURLs = ['http://roosterteeth.com/%s' % userTemp.lower(), 'http://roosterteeth.com/sponsRedir.php']

			try: 
				userlinks = comment.find_elements_by_xpath(".//a")
			except NoSuchElementException:
				userlinks = []
				pass
				
			for link in userlinks:
				tempLink = link.get_attribute('href')
				if tempLink:
					if tempLink not in excludeURLs and 'http://roosterteeth.com/members/comments/conversation.php?' not in tempLink:
						tempLinks.append(link.get_attribute('href'))

			try:
				flag = False
				end = 4
				for i in range(0, end):
					if not flag:
						tempParent = comment.find_element_by_xpath('..')
						flag = True
					else:
						tempParent = tempParent.find_element_by_xpath('..')

				replies = tempParent.find_elements_by_class_name('updateSingleReply')
				print('Replies exist!')
				for reply in replies:
					tempReplyLinks = []
					moreFlag = False
					try: 
						replyTemp = reply.find_element_by_xpath('.//table/tbody/tr/td[2]/div[2]/div[2]').text
						moreFlag = True
					except (NoSuchElementException, StaleElementReferenceException): 
						replyTemp = reply.find_element_by_xpath('.//table/tbody/tr/td[2]/div[2]').text
					print(replyTemp)  
					replyUserTemp = reply.find_element_by_xpath('.//table/tbody/tr/td[2]/div[1]/table/tbody/tr/td[1]/a').text
					print(replyUserTemp)
					replyDateTemp = reply.find_element_by_css_selector("span[class='updateWhen']").text
					print(replyDateTemp)
					if moreFlag:
						try:replyUserLinks = reply.find_element_by_xpath('.//table/tbody/tr/td[2]/div[2]/div[2]').find_elements_by_tag_name('a')
						except NoSuchElementException:
							pass
					else:
						try: replyUserLinks = reply.find_element_by_xpath('.//table/tbody/tr/td[2]/div[2]').find_elements_by_tag_name('a')
						except NoSuchElementException:
							pass
					if replyUserLinks:
						for link in replyUserLinks:
							tempReplyImgs.append(link.get_attribute('href'))
					repliesDict[replies.index(reply)] = [replyUserTemp, self.removeCommonTags(replyTemp), tempReplyLinks, replyDateTemp]
					print(tempReplyLinks)
			except NoSuchElementException:
				pass

			self.commentDict[self.commentStream.index(comment)] = [userTemp, self.removeCommonTags(commentTemp), tempLinks, postedDateTemp, repliesDict, self.currentdatetime()]

		return self.commentDict

	def journalStream(self, url='roosterteeth.com/burnie'):
		'''

		Pull all journals of user

		'''
		self.wait_for_internet()
		url = self.getPage(url.lower())
		try: 
			if self.driver.find_element_by_xpath('//*[@id="pageContent"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[3]/table[1]/tbody/tr/td/table/tbody/tr/td[1]/table/tbody/tr/td[1]').text.lower() != url.split('/')[3]:
				raise Exception('Incorrect URL for Roosterteeth User Page! Format is: ' + 'http://roosterteeth.com/username')
		except NoSuchElementException:
			raise Exception('Incorrect URL for Roosterteeth User Page! Format is: ' + 'http://roosterteeth.com/username')


		self.dataDict = {}
		self.journalDict = {}
		excludeURLs = ['http://roosterteeth.com/sponsRedir.php', url]
		seeMores = []
		seeMoresTemp = []
		tempLinks = []
		userlinks = []
		multPageFlag = False
		lastPage = 1

		try:
			self.wait_for_internet()
			with wait_for_page_load(self.driver):
				self.driver.find_element_by_id('profileNavjournal').click()
		except NoSuchElementException:
			return None

		self.contentZone = self.driver.find_element_by_id('innerRiver')

		try: 
			self.contentZone = self.driver.find_element_by_id('innerRiver')
			self.contentZone.find_element_by_xpath(".//table/tbody/tr/td/table/tbody/tr/td[2]/a")
			multPageFlag = True
		except NoSuchElementException:
			multPageFlag = False

		if multPageFlag:
			self.contentZone = self.driver.find_element_by_id('innerRiver')
			footerTemp = self.contentZone.find_elements_by_xpath('.//table/tbody/tr/td/table/tbody/tr/td[2]')
			footerTemp = footerTemp[len(footerTemp) - 1]
			pagesTemp = footerTemp.find_elements_by_tag_name("a")
			lastPageTemp = footerTemp.find_element_by_xpath('.//a[%s]' % len(pagesTemp))
			lastPage = int(lastPageTemp.text)

		self.contentZoneOld = False

		for i in range(0, lastPage):
			contFlag = False
			counterKeep = 0
			while not contFlag:
				try: 
					self.contentZone = self.driver.find_element_by_id('innerRiver')
					contFlag = True
				except StaleElementReferenceException:
					contFlag = False
					counterKeep += 1
					if counterKeep > 100: raise Exception('Error in finding journals - Stale element far too long!')
			counterKeep = 0
			if self.contentZoneOld:
				self.contentZone = self.driver.find_element_by_id('innerRiver')
				while self.contentZoneOld == self.contentZone.text:
					self.contentZone = self.driver.find_element_by_id('innerRiver')
					counterKeep += 1
					if counterKeep > 100: raise Exception('Error in finding journals - Stale element far too long!')
			journalsTemp = self.contentZone.find_elements_by_css_selector("td[class='streamMessage']")
			for td in journalsTemp:

				tempLink = ''
				tempLinks = []
				userlinks = []

				try:
					titleTemp = td.find_element_by_css_selector("a[class='streamTitle']").text
				except NoSuchElementException:
					titleTemp = ''
				headerTemp = td.find_element_by_xpath('.//table[1]')
				footerJTemp = td.find_element_by_xpath('.//table[2]')
				commentCount = footerJTemp.find_element_by_class_name('streamCommentCount').text
				dateTemp = footerJTemp.find_element_by_xpath('.//tbody/tr/td/a[1]/span').get_attribute('title')[:8]
				dateTemp = dateTemp.replace(' ', '')
				dateTemp = dateTemp.replace('/', ' ')
				if len(dateTemp) != 8:
					dateTemp = '0' + dateTemp #Make sure that the characters expected are there should months not be double digit already
				dateTemp = time.strptime(dateTemp, "%m %d %y")
				dateTemp = datetime.datetime(*dateTemp[:6])
				bodyTemp = td.text.replace(footerJTemp.text, '').replace(headerTemp.text, '').replace(titleTemp, '')
				try: 
					userlinks = td.find_elements_by_xpath(".//a")
				except NoSuchElementException:
					userlinks = []
					pass
					
				for link in userlinks:
					self.wait_for_internet()
					tempLink = link.get_attribute('href')
					if tempLink:
						if tempLink not in excludeURLs and 'http://roosterteeth.com/members/journal/entry.php?' not in tempLink and url not in tempLink:
							tempLinks.append(link.get_attribute('href'))
				self.journalDict[(((i - 1) * 8) + journalsTemp.index(td))] = [self.removeCommonTags(titleTemp), self.removeCommonTags(bodyTemp), tempLinks, dateTemp, commentCount]
			self.contentZoneOld = self.driver.find_element_by_id('innerRiver').text
			contFlag = False
			counterKeep = 0
			while not contFlag:
				try: 
					self.contentZone = self.driver.find_element_by_id('innerRiver')
					contFlag = True
				except StaleElementReferenceException:
					contFlag = False
					counterKeep += 1
					if counterKeep > 100: raise Exception('Error in finding journals - Stale element far too long!')
			footerTemp = self.contentZone.find_elements_by_xpath('.//table/tbody/tr/td/table/tbody/tr/td[2]')
			footerTemp = footerTemp[len(footerTemp) - 1]
			try: 
				footerTemp.find_element_by_xpath('../td[3]/a').click()
				print('Next page!')
			except NoSuchElementException: 
				print('No more pages!')
				break

		return self.journalDict

def demo():
	user = userData()
	testBio = user.bio('www.roosterteeth.com/burnie')
	testComments = user.commentStream('www.roosterteeth.com/burnie')
	testJournals = user.journalStream('www.roosterteeth.com/burnie')
	user.shutDown() #Close webdriver
	print(testBio)
	print(testJournals)
	print(testJournals)

demo()