import requests
import bs4
from bs4 import Comment
from bs4 import NavigableString
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException 
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import ElementNotVisibleException
import csv
import time
import os
import re
import gspread
from oauth2client.client import GoogleCredentials 
import sys
import datetime
import json


def find_data_file(filename):
	if getattr(sys, 'frozen', False):
		# The application is frozen
		datadir = os.path.dirname(sys.executable)
	else:
		# The application is not frozen
		# Change this bit to match where you store your data files:
		datadir = os.path.dirname(os.path.abspath(__file__)) #This grabs the directory of the script for finding the needed things more easily
	return os.path.join(datadir, filename)

def internet_on(url='http://www.google.com/', timeout=5):
	try:
		_ = requests.get(url, timeout=timeout)
		return True
	except requests.ConnectionError:
		print('No internet connection available')
	return False

def wait_for_internet():
	count = 0
	while not internet_on():
		count += 1
		if count > 120:
			print('Timed out! Request took longer than ten minutes!')
			sys.exit()

def wait_for_pop(condition_function, string):
	start_time = time.time()
	while time.time() < start_time + 60:
		try: 
			condition_function(string)
			return True
		except NoSuchElementException:
			time.sleep(0.1)
	raise Exception('Timeout waiting for {}'.format(condition_function.__name__))

def wait_for(condition_function): #The wait function used in the page load wait class
	start_time = time.time()
	while time.time() < start_time + 60:
		if condition_function():
			return True
		else:
			time.sleep(0.1)
	raise Exception('Timeout waiting for {}'.format(condition_function.__name__))
		
class wait_for_page_load(object): #Needed to add sufficient waits based on page element staleness - or if an element can no longer be found

	def __init__(self, driver): #Designate browser
		self.driver = driver

	def __enter__(self): #Find the element
		self.old_page = self.driver.find_element_by_tag_name('html')

	def page_has_loaded(self): #Check for staleness
		new_page = self.driver.find_element_by_tag_name('html')
		return new_page.id != self.old_page.id

	def __exit__(self, *_): #Wait and then exit class with
		wait_for(self.page_has_loaded)
		
	'''
	You use it like this -
	with wait_for_page_load(driver):
		driver.find_element_by_link_text('my link').click()
	'''

def removeCommonTags(stringVal):
	# Remove misc tags
	tempString = stringVal.replace("\n"," ")
	tempString = tempString.replace("\t"," ")
	tempString = tempString.replace("\xa0"," ")
	tempString = tempString.replace("\u25cf"," ")
	tempString = tempString.replace('[if !supportLists]'," ")
	tempString = tempString.replace('[endif]'," ")

	return tempString

def removeMiscTags(stringVal):

	listTemp = re.sub('(\<.*?\>)', '', stringVal)

	return listTemp

def removeExtSpaces(stringVal):

	listTemp = re.sub(' +',' ', stringVal) # Remove extra spaces

	return listTemp

def CSVMaker(dataDict, headers=[], csvName=('RT_Data' + str(time.strftime("%d-%m-%Y")))):
		
	skipList = []
	listTemp = []
	csvFileOut = (os.path.expanduser('~\\RTData\\') + csvName+ '.csv')

	if not os.path.exists(os.path.expanduser('~\\RTData\\')):
	    os.makedirs(os.path.expanduser('~\\RTData\\'))

	if not os.path.exists(csvFileOut):

		with open(csvFileOut, 'w', newline='') as output:
			writer = csv.writer(output, delimiter=',', quoting=csv.QUOTE_ALL)
			
			writer.writerow(headers)

			for key, item in dataDict.items():
				listTemp = item
				writer.writerow(listTemp)

	else:

		with open(csvFileOut, newline='') as csvfile:
			reader = csv.reader(csvfile, delimiter=',')
			for row in reader:
				for key, item in dataDict.items():
					if len(item) > 2:
						if item[0] in row and item[1] in row and item[2] in row:
							skipList.append(key)
					else:
						if item[0] in row and item[1] in row:
							skipList.append(key)


		with open(csvFileOut, 'a', newline='') as output:
			writer = csv.writer(output, delimiter=',', quoting=csv.QUOTE_ALL)

			for key, item in dataDict.items():
				if key not in skipList:
					listTemp = item
					writer.writerow(listTemp)

def googleSpread(WBName, checkVal, dataDict, headers=[]):

	'''
	Adds rows to google drive spreadsheet 
	'''

	print('Google update start...')

	wait_for_internet()

	'''
	# Check out these links for more info
	http://gspread.readthedocs.org/en/latest/oauth2.html for more info on how this authentication works
	https://github.com/burnash/gspread/issues/224
	'''
	#This works like this os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "path_to_file_on_pc"
	os.environ['GOOGLE_APPLICATION_CREDENTIALS'] =  find_data_file('')
	
	credentials = GoogleCredentials.get_application_default()
	credentials = credentials.create_scoped(['https://spreadsheets.google.com/feeds'])
	gc = gspread.authorize(credentials)

	''' #DEPRICATED in GSpread# - This is replaced with OAuth2Client above
	spreadsheet_key = '' #Unique to each sheet
	email = '' #Google account username
	password = '' #Application level pass
	worksheet_id = 'od6' #Generally the ID of the first sheet
	gc = gspread.login(email, password)
	'''
	wks = gc.open(WBName).sheet1

	if checkVal not in wks.row_values(1):
		print('Adding headers')
		wks.resize(1)
		for i in range(0, len(headers)):
			wks.update_cell(1,(i+1), headers[i])

	for key, item in dataDict.items():

		print('Checking row...')

		flag = False
		listTemp = item

		if headers[0] == 'Header':
			
			wait_for_internet()

			allCells = wks.get_all_values() #Is it a list of rows (lists of cell values)?
			for row in allCells:
				if item[0] == row[0] and item[1] == row[1] and item[2] == row[2] and item[4] == row[4]:
					flag = True
					break

		elif headers[0] == 'URL':

			wait_for_internet()

			allCells = wks.get_all_values() #Is it a list of rows (lists of cell values)?
			for row in allCells:
				if item[0] == row[0]:
					flag = True
					break

		if headers[0] == 'User' or headers[0] == 'Date':

			wait_for_internet()

			allCells = wks.get_all_values()
			for row in allCells:
				if item[0] == row[0] and item[1] == item[1]:
					flag = True
					break


		if flag: continue

		print('Appending Row!')
		wks.append_row(listTemp)

def careerPull():

	'''
	Gets any new careers and makes a CSV/Google sheet update
	'''

	listPages = []
	titledivs = ''
	bodydivs = ''
	counter = 0
	count = 0
	textList = []
	listTemp = []
	skipList = []
	lister = []
	infoDict = {}
	flag = False

	for i in range(1, 100000):
		listPages.append('http://roosterteeth.com/careers/position.php?id=%s' % i)

	print('Done making URLs!')

	for page in listPages:

		listTemp = []
		count = 0

		wait_for_internet()

		response = requests.get(page)
		print('Gotten page!')
		soup = bs4.BeautifulSoup(response.text)
		titledivs = soup.find("div", { "class" : "titleLine" }).get_text()
		print(titledivs)
		if titledivs != ' -- ': flag = True
		else: print('No info - moving on')
		if titledivs == ' -- ' and flag:
			print('No Info')
			counter += 1
			if counter > 10: break
			continue
		elif titledivs != ' -- ' and flag:
			counter = 0
		else:
			counter += 1
			if counter > 25:
				print('You need to push up the start point of the range by 25!')
				break
			continue

		textList = soup.find("div", { "class" : "small" }).findAll(text=True)
		if soup.find("div", { "class" : "small" }).find('p', {'class': 'MsoNormal'}):
			print('mso in the post')
			textListOrig = soup.find("div", { "class" : "small" }).findAll(text=True)
			textList = [x.encode('utf-8').decode('utf-8') for x in textListOrig if not isinstance(x, Comment)]
			#textList = 
			#textListOrig = soup.find("div", { "class" : "small" }).findAll(text=True)
			#textList += [x.get_text() for x in textListOrig]
			
		for x in textList:
			if x.find('span'):
				listTemp.append(removeCommonTags(x.encode('utf-8').decode('utf-8')))
			else: listTemp.append(removeCommonTags(x))

		listTemp = ' '.join(listTemp) # change list of strings to single string
		listTemp = removeMiscTags(listTemp)
		listTemp = removeExtSpaces(listTemp)
		if 'Pellentesque habitant' in listTemp: continue
		if 'Test Post' in listTemp: continue
		if not listTemp: continue

		print('Adding career to dictionary')

		infoDict[listPages.index(page)+1] = [page, titledivs, listTemp, time.strftime("%d-%m-%Y")]

	csvName = 'RTCareers' + '_Data'
	headers = ['URL', 'Title of Role', 'Body Text', 'Date']
	CSVMaker(infoDict, headers, csvName)
	print('CSV Done')
	checkVal = 'URL'
	WBName = 'RT Career Data'
	googleSpread(WBName, checkVal, infoDict, headers=headers)
	print('Successful Career Data Gather!')
	return infoDict

def topPostPull():

	'''
	Gets the top posts for the day and outpus to CSV/Google sheet
	'''

	print('Pull top post')

	topPostDict = {}
	tempPostList = []

	wait_for_internet()

	response = requests.get('http://roosterteeth.com/members/stats/top.php')
	print('Gotten page!')
	soup = bs4.BeautifulSoup(response.text)
	journalDivs = soup.findAll('div', { "class" : "post" })

	for div in journalDivs:

		tempPostList = []

		if div.find('h2') == None or not div.find('h2'):
			tempPostList.append(' ')
		else:
			tempString = div.find('h2').get_text()
			tempString = removeCommonTags(tempString)
			tempString = removeExtSpaces(tempString)
			tempString = tempString.encode('ascii','ignore')
			tempPostList.append(tempString.decode("utf-8"))

		if div.find('div', {'class' : 'article_body'}) == None or not div.find('div', {'class' : 'article_body'}):
			continue
		tempString = div.find('div', { "class" : "article_body" }).get_text()
		tempString = removeCommonTags(tempString)
		tempString = removeExtSpaces(tempString)
		tempString = tempString.encode('ascii','ignore')
		tempPostList.append(tempString.decode("utf-8"))

		tempString = div.find('div', {"class" : "postFooter"}).get_text()
		tempString = removeCommonTags(tempString)
		tempString = removeMiscTags(tempString)
		tempString = removeExtSpaces(tempString)
		poster = tempString.split("ago by ", 1)[1]
		commentCount = poster.split(" ")[3]
		commentCount = commentCount.replace(")","")
		commentCount = commentCount.replace("(","")
		poster = poster.split(" ")[0]
		tempPostList.append(poster)
		tempPostList.append(abs(float(commentCount)))

		tempPostList.append(time.strftime("%d-%m-%Y"))
		if not tempPostList[0] or tempPostList[0] == ' ':
			tempPostList[0] = 'Default Val'
		if not tempPostList[1] or tempPostList[1] == ' ':
			tempPostList[1] = '[Image]/[Misc]'


		topPostDict[journalDivs.index(div)] = tempPostList

	csvName = 'RTTopPost' + '_Data'
	headers = ['Header', 'Post Body', 'Poster', 'Comment Count', 'Date']
	CSVMaker(topPostDict, headers, csvName)
	checkVal = 'Post Body'
	WBName = 'RT Top Post Data'
	googleSpread(WBName, checkVal, topPostDict, headers=headers)
	print('Successful Top Post Data Gather!')
	return topPostDict

def mostLikedPull():

	'''
	Gets the top 12 most liked users of the last 24 hours at any one time
	'''

	print('Pull most liked')

	likedUsersDict = {}
	likedUsers = []

	wait_for_internet()
	
	response = requests.get('http://roosterteeth.com/members/stats/')
	print('Gotten page!')
	soup = bs4.BeautifulSoup(response.text)
	divTopUsers = soup.find('div', {"id" : "Most Liked Members (Last 7 Days)"})
	topUsers = divTopUsers.findAll('a', { "class" : "hi" })

	for user in topUsers:
		likedUsersDict[user.get_text()] = [user.get_text(), time.strftime("%d-%m-%Y")]
		likedUsers.append(user.get_text())

	csvName = 'RTTopLikedUsers' + '_Data'
	headers = ['User', 'Date']
	CSVMaker(likedUsersDict, headers, csvName)
	checkVal = 'User'
	WBName = 'RT Most Liked'
	googleSpread(WBName, checkVal, likedUsersDict, headers=headers)
	print('Successful Most Liked User Data Gather!')
	return likedUsersDict

def featuredUserPull():

	'''
	Gets the featured user and returns it as well as places it in CSV/Google sheet
	'''
	print('Pulling featured')

	featuredUser = ''
	featuredUserDict = {}

	wait_for_internet()
	
	response = requests.get('http://roosterteeth.com')
	print('Gotten page!')
	soup = bs4.BeautifulSoup(response.text)
	sideBar = soup.find('td', {"id" : "sidebar"})
	tableLocation = soup.find('table', {"class" : "web2User"})
	featuredUser = sideBar.find('a', {"class" : "hi"}, text=True).get_text()
	featuredUserDict[time.strftime("%d-%m-%Y")] = [time.strftime("%d-%m-%Y"), featuredUser]

	csvName = 'RTFeaturedUser' + '_Data'
	headers = ['Date', 'User']
	CSVMaker(featuredUserDict, headers, csvName)
	checkVal = 'User'
	WBName = 'RT Featured Users'
	googleSpread(WBName, checkVal, featuredUserDict, headers=headers)
	print('Successful Featured User User Gather!')
	return featuredUserDict

if __name__ == '__main__':

	os.system("TASKKILL /F /IM phantomjs.exe")
	careerPull()
	topPostPull()
	mostLikedPull() #Gather top five to ten users and return their data via the userData class as CSV and google sheet
	featuredUserPull() #Gather featured user's URL and return their data via the userData class as CSV and google sheet