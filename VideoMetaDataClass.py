import requests
import bs4
from bs4 import Comment
from bs4 import NavigableString
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException 
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import ElementNotVisibleException
import csv
import time
import os
import re
import gspread
import sys
import datetime
from datetime import timedelta as tdel
from datetime import date
from dateutil.relativedelta import relativedelta

def internet_on(url='http://www.google.com/', timeout=5):
	try:
		_ = requests.get(url, timeout=timeout)
		return True
	except requests.ConnectionError:
		print('No internet connection available')
	return False

def wait_for_internet():
	count = 0
	while not internet_on():
		count += 1
		if count > 120:
			print('Timed out! Request took longer than ten minutes!')
			exit()

def find_data_file(filename):
	if getattr(sys, 'frozen', False):
		# The application is frozen
		datadir = os.path.dirname(sys.executable)
	else:
		# The application is not frozen
		# Change this bit to match where you store your data files:
		datadir = os.path.dirname(os.path.abspath(__file__)) #This grabs the directory of the script for finding the needed things more easily
	return os.path.join(datadir, filename)

def wait_for_pop(condition_function, string):
	start_time = time.time()
	while time.time() < start_time + 60:
		try: 
			condition_function(string)
			return True
		except NoSuchElementException:
			time.sleep(0.1)
	raise Exception('Timeout waiting for {}'.format(condition_function.__name__))

def wait_for(condition_function): #The wait function used in the page load wait class
	start_time = time.time()
	while time.time() < start_time + 180:
		if condition_function():
			return True
		else:
			time.sleep(0.1)
	raise Exception('Timeout waiting for {}'.format(condition_function.__name__))
		
class wait_for_page_load(object): #Needed to add sufficient waits based on page element staleness - or if an element can no longer be found

	def __init__(self, driver): #Designate browser
		self.driver = driver

	def __enter__(self): #Find the element
		self.old_page = self.driver.find_element_by_tag_name('html')

	def page_has_loaded(self): #Check for staleness
		new_page = self.driver.find_element_by_tag_name('html')
		return new_page.id != self.old_page.id

	def __exit__(self, *_): #Wait and then exit class with
		wait_for(self.page_has_loaded)
		
	'''
	You use it like this -
	with wait_for_page_load(driver):
		driver.find_element_by_link_text('my link').click()
	'''

class mixedMeta(object):

	def __init__(self):
		#self.driver = webdriver.Firefox()
		flag = False
		counterKeep = 0
		while flag == False:
			try: 
				self.driver = webdriver.PhantomJS(executable_path=find_data_file('phantomjs.exe'))
				#Phantom is nearly three times faster than a visual browser
				flag = True
			except WebDriverException:
				counterKeep += 1
				flag = False
			if counterKeep > 100: raise Exception('Unable to start driver!')
		self.driver.set_window_size(1920, 1080)

	def check_by_name(self, name):
		try: self.driver.find_element_by_name(name)
		except NoSuchElementException:
			return False
		return True	
		
	def check_by_xpath(self, path):
		try:self.driver.find_element_by_xpath(path)
		except NoSuchElementException:
			return False
		return True

	def check_by_id(self, name):
		try:self.driver.find_element_by_id(name)
		except NoSuchElementException:
			return False
		return True

	def check_by_link_text(self, text):
		try:self.driver.find_element_by_link_text(text)
		except NoSuchElementException:
			return False
		return True

	def check_by_class(self, classStr):
		try:self.driver.find_element_by_class_name(classStr)
		except NoSuchElementException:
			return False
		return True

	def getPage(self, url='http://roosterteeth.com/archive/?id=10588&v'):
		if 'www.youtube' not in url.lower() and 'http://' not in url.lower():
			url = 'http://' + url
		elif 'www.youtube' in url.lower() and 'https://' not in url.lower():
			url = 'https://' + url

		if 'www.roosterteeth' in url.lower():
			if 'archive/?id=' not in url.lower() or 'archive/?sid=' not in url.lower():
				raise Exception('Incorrect URL for Roosterteeth Video! Format is: ' + 'http://roosterteeth.com/archive/?id=10588&v')
		
		if 'www.youtube' in url.lower():
			if 'watch?v=' not in url.lower():
				raise Exception('Incorrect URL for YouTube Video! Format is: ' + 'https://www.youtube.com/watch?v=qyou27LvCQw')

		if 'www.reddit' in url.lower():
			if '/r/' not in url.lower() or '/comments/' not in url.lower():
				raise Exception('Incorrect URL for Reddit Page! Format is: ' + 'http://www.reddit.com/r/roosterteeth/comments/33r5vp/gus_against_humanity_dirty_bits_rt_podcast_314/')

		self.driver.get(url)
		print('Got page!')
		return url

	def shutDown(self):
		self.driver.quit()

	def currentdatetime(self):
		return datetime.datetime.now()

	def removeCommonTags(self, stringVal):
		# Remove misc tags
		tempString = stringVal.replace("\n"," ")
		tempString = tempString.replace("\t"," ")
		tempString = tempString.replace("\xa0"," ")
		tempString = tempString.replace("\u25ba"," ")
		tempString = tempString.replace("\u0336"," ")
		tempString = tempString.replace('[if !supportLists]'," ")
		tempString = tempString.replace('[endif]'," ")
		tempString = re.sub(r'[\']', "'", tempString)

		return tempString

	def pullComments(self, url='http://roosterteeth.com/archive/?id=10588&v'):

		'''

		Pull comment content of all comments as well as who posted it, when it was posted, and the upvotes/downvotes/likes/score if applicable (on RT you can't see this without logging in, so it isn't), and whether it was gelded
		This also should pull the visible children comments and their meta data

		'''

		flagType = ''
		flagStop = False
		self.commentDict = {}

		wait_for_internet()

		url = self.getPage(url)
		waitCount = 0

		print(url)

		if 'www.roosterteeth' in url.lower() or 'http://roosterteeth' in url.lower():
			flagType = 'RT'
			if not self.check_by_id('socialVideo'):
				raise Exception('Incorrect URL for Roosterteeth Video Page! Format is: ' + 'http://roosterteeth.com/archive/?id=videoID')
		elif 'www.youtube' in url.lower() or 'https://youtube' in url.lower():
			flagType = 'YT'
			while not self.check_by_id('watch-discussion'):
				waitCount += 1
				if waitCount > 50:
					raise Exception('Incorrect URL for YouTube Video Page! Format is: ' + 'https://www.youtube.com/watch?v=videoID')
		elif 'www.reddit' in url.lower() or 'http://reddit' in url.lower():
			flagType = 'RD'
			if not self.check_by_class('commentarea'):
				raise Exception('Incorrect URL for Reddit Page! Format is: ' + 'http://www.reddit.com/r/subreddit/comments/pageID/nameTag')
		else:
			raise Exception('Error in parsing URL. Please be sure it is a Reddit post, youtube video, or RT video URL')


		if flagType == 'RT':

			print('Rooster Teeth!')

			mainPageID = self.driver.find_element_by_id('socialVideo').get_attribute('data-id')
			mainButton = self.driver.find_element_by_css_selector('a[href="/archive/?id=%s"]' % mainPageID).click()
			

			multPageFlag = False
			flagDone = False
			waitCount = 0
			lastPage = 1
			print('RT Link')

			while not self.check_by_id('commentsAreaHold'):
				self.driver.find_element_by_id('profileNavcomments').click()
				print('Clicked Comments')
				waitCount += 1
				if waitCount > 100: 
					raise Exception('Timed out getting into comments ajax of RT page')
			
			try: 
				wait_for_internet()
				while not flagDone:
					try: 
						self.commentsZone = self.driver.find_element_by_id('commentsAreaHold')
						self.commentsZone.find_element_by_xpath(".//div[2]/table/tbody/tr/td/table/tbody/tr/td[2]/a[2]")
						flagDone = True
					except StaleElementReferenceException:
						flagDone = False
				multPageFlag = True
			except NoSuchElementException:
				multPageFlag = False
			
			if multPageFlag:
				wait_for_internet()
				contFlag = False
				counterKeep = 0
				while not contFlag:
					try: 
						self.commentsZone = self.driver.find_element_by_id('commentsAreaHold')
						contFlag = True
					except StaleElementReferenceException: 
						contFlag = False
						counterKeep += 1
						if counterKeep > 100: raise Exception('Error in finding comments - Stale element far too long!')
				footerTemp = self.commentsZone.find_element_by_xpath('.//div[2]/table/tbody/tr/td/table/tbody/tr/td[2]')
				pagesTemp = footerTemp.find_elements_by_tag_name("a")
				lastPage = int(footerTemp.find_element_by_xpath('.//a[%s]' % len(pagesTemp)).text)
				print('Many Pages :' + str(lastPage))

			self.commentsZoneOld = False

			for i in range(0, lastPage + 1):
				print('Page: ' + str(i+1))
				contFlag = False
				passFlag = False
				counterKeep = 0
				while not contFlag:
					try: 
						self.commentsZone = self.driver.find_element_by_id('commentsArea')
						contFlag = True
					except (StaleElementReferenceException, NoSuchElementException) as e: 
						contFlag = False
						counterKeep += 1
						if counterKeep > 100: raise Exception('Error in finding comments - Stale element far too long!')
				counterKeep = 0
				if self.commentsZoneOld:
					self.commentsZone = self.driver.find_element_by_id('commentsArea')
					while self.commentsZoneOld == self.commentsZone.text:
						while not passFlag:
							try: 
								self.commentsZone = self.driver.find_element_by_id('commentsArea')
								passFlag = True
							except NoSuchElementException: 
								passFlag = False
						counterKeep += 1
						if counterKeep > 100: raise Exception('Error in finding comments - Stale element far too long!')
				else: time.sleep(2)
				commentsTemp = self.commentsZone.find_elements_by_css_selector("td[class='streamMessage']")
				for td in commentsTemp:
					print('Comment: ' + str(commentsTemp.index(td)))

					tempLink = ''
					tempLinks = []
					userlinks = []

					userTemp = td.find_element_by_xpath('.//table[1]/tbody/tr/td[1]/a').text
					dateTemp = td.find_element_by_css_selector("span[class='updateWhen']").get_attribute('title')
					dateTemp = dateTemp[:8]
					dateTemp = dateTemp.replace(' ', '')
					dateTemp = dateTemp.replace('/', ' ')
					if len(dateTemp) != 8:
						dateTemp = '0' + dateTemp #Make sure that the characters expected are there should months not be double digit already
					dateTemp = time.strptime(dateTemp, "%m %d %y")
					dateTemp = datetime.datetime(*dateTemp[:6])
					commentTemp = td.text
					removeTemp = td.find_element_by_xpath('.//table[2]').text
					commentTemp = commentTemp.replace(removeTemp,'')
					removeTemp = td.find_element_by_xpath('.//table[1]').text
					commentTemp = commentTemp.replace(removeTemp,'')
					excludeURLs = ['http://roosterteeth.com/%s' % userTemp.lower(), 'http://roosterteeth.com/sponsRedir.php']

					try: 
						userlinks = td.find_elements_by_xpath(".//a")
					except NoSuchElementException:
						userlinks = []
						pass
						
					for link in userlinks:
						tempLink = link.get_attribute('href')
						if tempLink:
							if tempLink not in excludeURLs and 'http://roosterteeth.com/members/comments/conversation.php?' not in tempLink:
								tempLinks.append(link.get_attribute('href'))

					self.commentDict[((30 * i) + commentsTemp.index(td))] = [userTemp, self.removeCommonTags(commentTemp).encode('utf-8').decode('utf-8'), tempLinks, dateTemp]
				self.commentsZoneOld = self.driver.find_element_by_id('commentsArea').text
				contFlag = False
				counterKeep = 0
				while not contFlag:
					try: 
						self.commentsZoneOver = self.driver.find_element_by_id('commentsAreaHold')
						contFlag = True
					except StaleElementReferenceException: 
						contFlag = False
						counterKeep += 1
						if counterKeep > 100: raise Exception('Error in finding comments - Stale element far too long!')
				self.commentsZoneOver = self.driver.find_element_by_id('commentsAreaHold')
				footerTemp = self.commentsZoneOver.find_elements_by_xpath('.//div[2]/table/tbody/tr/td/table/tbody/tr/td[3]')
				footerTemp = footerTemp[len(footerTemp) - 1]
				print(footerTemp.text)
				try: 
					wait_for_internet()
					footerTemp.find_element_by_link_text('Next').click()
					print('Next page!')
				except NoSuchElementException: 
					print('No more pages!')
					break

		if flagType == 'YT':

			print('YouTube!')

			waitCount = 0

			while not self.check_by_id('yt-comments-list'):
				self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
				waitCount += 1
				if waitCount > 20: raise Exception("Scroll error! Webdriver can't seem to scroll...")

			waitCount = 0
			while not flagStop:
				print('Attempting to click more!')
				try: 
					showMore = self.driver.find_element_by_id('yt-comments-paginator').click()
					print('Clicked more!')
					waitCount = 0
					flagStop = False
				except NoSuchElementException:
					waitCount += 1
					if waitCount > 20:
						print('No more click mores...')
						flagStop = True
				except ElementNotVisibleException:
					print('No more click mores...')
					break

			try: self.showMores = self.driver.find_elements_by_class_name('show-more')
			except NoSuchElementException: self.showMores = []

			waitCount = 0
			contFlag = False
			if self.showMores:
				while not contFlag:
					try: 
						self.showMores = self.driver.find_elements_by_class_name('show-more')
						self.showMores = [x for x in self.showMores if x.is_displayed()]
						contFlag = True
					except StaleElementReferenceException:
						contFlag = False
						waitCount += 1
						if waitCount > 25: raise Exception('DOM element stale error!')

				print('Show Mores Exist!')
				for x in self.showMores:
					flagStop = False
					while not flagStop:
						try: 
							print('Show More: ' + str(self.showMores.index(x)))
							x.click()
							print('Clicked!')
							flagStop = True
						except ElementNotVisibleException:
							flagStop = True

			try: self.expands = self.driver.find_elements_by_class_name('expand')
			except NoSuchElementException: self.expands = []

			waitCount = 0
			contFlag = False
			if self.expands:
				while not contFlag:
					try: 
						self.expands = self.driver.find_elements_by_class_name('expand')
						self.expands = [x for x in self.expands if x.is_displayed()]
						contFlag = True
					except StaleElementReferenceException: 
						contFlag = False
						waitCount += 1
						if waitCount > 25: raise Exception('DOM element stale error!')

				print('Expands Exist!')
				for x in self.expands:
					flagStop = False
					while not flagStop:
						try: 
							print('Read More: ' + str(self.expands.index(x)))
							x.click()
							print('Clicked!')
							flagStop = True
						except ElementNotVisibleException:
							flagStop = True

			self.commentsZone = self.driver.find_element_by_id('yt-comments-list')
			self.commentsMain = self.commentsZone.find_elements_by_class_name('comment-entry')

			for x in self.commentsMain:
				index = self.commentsMain.index(x)
				print('Comment: ' + str(index))
				tempLinks = []
				childrenTempDict = {}
				parentTempUser = x.find_element_by_xpath('.//div[1]/div[1]/div[1]/a').text
				parentTempDate = x.find_element_by_xpath('.//div[1]/div[1]/div[1]/span[2]/a').text
				if 'year' in parentTempDate:
					parentTempDate = int(re.sub("[^0-9]", "", parentTempDate))
					parentTempDate = datetime.datetime.now() - relativedelta(months = -(parentTempDate * 12))
				elif 'month' in parentTempDate:
					parentTempDate = int(re.sub("[^0-9]", "", parentTempDate))
					parentTempDate = datetime.datetime.now() - relativedelta(months = -parentTempDate)
				elif 'week' in parentTempDate:
					parentTempDate = int(re.sub("[^0-9]", "", parentTempDate))
					parentTempDate = datetime.datetime.now() - tdel(days = parentTempDate * 7)
				elif 'day' in parentTempDate:
					parentTempDate = int(re.sub("[^0-9]", "", parentTempDate))
					parentTempDate = datetime.datetime.now() - tdel(days = parentTempDate)
				elif 'hour' in parentTempDate:
					parentTempDate = int(re.sub("[^0-9]", "", parentTempDate))
					parentTempDate = datetime.datetime.now() - tdel(hours = parentTempDate)
				elif 'minute' in parentTempDate:
					parentTempDate = int(re.sub("[^0-9]", "", parentTempDate))
					parentTempDate = datetime.datetime.now() - tdel(minutes = parentTempDate)
				elif 'second' in parentTempDate:
					parentTempDate = int(re.sub("[^0-9]", "", parentTempDate))
					parentTempDate = datetime.datetime.now() - tdel(seconds = parentTempDate)
				try:parentTempScore = x.find_element_by_xpath('.//div[1]/div[1]/div[3]/div[1]').find_element_by_xpath('.//span[contains(@class, "like-count off")]').text
				except NoSuchElementException: parentTempScore = str(0)
				try: parentTempBody = x.find_element_by_xpath('.//div[1]/div[1]/div[2]/div[1]').text
				except NoSuchElementException: parentTempBody = ''
				try: parentTempLinks = x.find_elements_by_xpath('.//div[1]/div[1]/div[2]/div[1]/a')
				except NoSuchElementException: parentTempLinks = []
				if parentTempLinks:
					for x in parentTempLinks:
						tempLinks.append(x.get_attribute('href'))
						parentTempBody = parentTempBody.replace(x.text, '')

				try: 
					x.find_element_by_xpath('.//div[2]/div[1]')
					childrenTemp = x.find_elements_by_xpath('.//div[2]/div[contains(@class, "comment-item")]')
					for child in childrenTemp:
						childIndex = childrenTemp.index(child)
						tempLinksRepl = []
						contentTemp = child.find_element_by_class_name('content')
						childTempUser = contentTemp.find_element_by_xpath('.//div[1]/a').text
						childTempDate = contentTemp.find_element_by_xpath('.//div[1]/span[2]/a').text
						if 'year' in childTempDate:
							childTempDate = int(re.sub("[^0-9]", "", childTempDate))
							childTempDate = datetime.datetime.now() - relativedelta(months = -(childTempDate * 12))
						elif 'month' in childTempDate:
							childTempDate = int(re.sub("[^0-9]", "", childTempDate))
							childTempDate = datetime.datetime.now() - relativedelta(months = -childTempDate)
						elif 'week' in childTempDate:
							childTempDate = int(re.sub("[^0-9]", "", childTempDate))
							childTempDate = datetime.datetime.now() - tdel(days = childTempDate * 7)
						elif 'day' in childTempDate:
							childTempDate = int(re.sub("[^0-9]", "", childTempDate))
							childTempDate = datetime.datetime.now() - tdel(days = childTempDate)
						elif 'hour' in childTempDate:
							childTempDate = int(re.sub("[^0-9]", "", childTempDate))
							childTempDate = datetime.datetime.now() - tdel(hours = childTempDate)
						elif 'minute' in childTempDate:
							childTempDate = int(re.sub("[^0-9]", "", childTempDate))
							childTempDate = datetime.datetime.now() - tdel(minutes = childTempDate)
						elif 'second' in childTempDate:
							childTempDate = int(re.sub("[^0-9]", "", childTempDate))
							childTempDate = datetime.datetime.now() - tdel(seconds = childTempDate)
						try:childTempScore = contentTemp.find_element_by_xpath('.//div[3]/div[1]/span[contains(@class, "like-count off")]').text
						except NoSuchElementException: childTempScore = str(0)
						try:childTempBody = contentTemp.find_element_by_xpath('.//div[2]/div[1]').text
						except NoSuchElementException: childTempBody = ''
						try: childTempLinks = contentTemp.find_elements_by_xpath('.//div[2]/div[1]/a')
						except NoSuchElementException: childTempLinks = []
						if childTempLinks:
							for link in childTempLinks:
								tempLinksRepl.append(link.get_attribute('href'))
								childTempBody = childTempBody.replace(link.text, '')

						childrenTempDict['%s-%s' % (index, childIndex)] = [self.removeCommonTags(childTempUser), self.removeCommonTags(childTempBody).encode('utf-8').decode('utf-8'), childTempDate, childTempScore, tempLinksRepl]

				except NoSuchElementException: childrenTempDict = {}
				
				self.commentDict[index] = [self.removeCommonTags(parentTempUser), self.removeCommonTags(parentTempBody).encode('utf-8').decode('utf-8'), parentTempDate, parentTempScore, tempLinks, childrenTempDict]

		if flagType == 'RD':

			print('Reddit!')

			self.commentsZoneOver = self.driver.find_element_by_class_name('commentarea')
			self.commentsZone = self.commentsZoneOver.find_element_by_xpath('.//div[contains(@id, "siteTable")]')
		
			self.commentsStream = self.commentsZone.find_elements_by_css_selector("div[onclick='click_thing(this)']")
			self.mainComments = [x for x in self.commentsStream if x.find_element_by_xpath('..').find_element_by_xpath('..').get_attribute('class') == 'commentarea']
			'''
			Check if a step back using xpath has the attribute 'commentarea' and you can find all direct replies to post (and their children!)
			'''
			for x in self.mainComments:
				childrenTempDict = {}
				try: parentTempUser = x.find_element_by_xpath('.//div[contains(@class, "entry unvoted")]').find_element_by_xpath('.//a[contains(@class, "author")]').text
				except NoSuchElementException: parentTempUser = None
				try:
					parentTempDate = x.find_element_by_xpath('.//div[contains(@class, "entry unvoted")]').find_element_by_xpath('.//time[contains(@class, "live-")]').get_attribute('datetime') #Like 2015-04-29T16:38:03+00:00 - Break up into date and time and form to datetime type
					parentTempDate = re.sub('T', ' ', parentTempDate)
					parentTempDate = parentTempDate.split('+')[0]
					parentTempDate = time.strptime(parentTempDate, "%Y-%m-%d %H:%M:%S")
				except NoSuchElementException: parentTempDate = None
				try: parentTempScore = x.find_element_by_xpath('.//div[contains(@class, "entry unvoted")]').find_element_by_xpath('.//span[contains(@class, "score unvoted")]').text
				except NoSuchElementException: parentTempScore = None
				try: parentTempBody = x.find_element_by_xpath('.//div[contains(@class, "entry unvoted")]').find_element_by_class_name('usertext').find_element_by_xpath('.//div[1]').text
				except NoSuchElementException: parentTempBody = None
				try: 
					self.childrenTemp = x.find_elements_by_css_selector("div[onclick='click_thing(this)']")
					for sub in self.childrenTemp:
						try: childTempUser = sub.find_element_by_xpath('.//div[contains(@class, "entry unvoted")]').find_element_by_xpath('.//a[contains(@class, "author")]').text
						except NoSuchElementException: childTempUser = None
						try:
							childTempDate = sub.find_element_by_xpath('.//div[contains(@class, "entry unvoted")]').find_element_by_xpath('.//time[contains(@class, "live-")]').get_attribute('datetime') #Like 2015-04-29T16:38:03+00:00 - Break up into date and time and form to datetime type
							childTempDate = re.sub('T', ' ', childTempDate)
							childTempDate = childTempDate.split('+')[0]
							childTempDate = time.strptime(childTempDate, "%Y-%m-%d %H:%M:%S")
						except NoSuchElementException: childTempDate = None
						try: childTempScore = sub.find_element_by_xpath('.//div[contains(@class, "entry unvoted")]').find_element_by_xpath('.//span[contains(@class, "score unvoted")]').text
						except NoSuchElementException: childTempScore = None
						try: childTempBody = sub.find_element_by_xpath('.//div[contains(@class, "entry unvoted")]').find_element_by_class_name('usertext').find_element_by_xpath('.//div[1]').text
						except NoSuchElementException: childTempBody = None
						if childTempUser == parentTempUser and childTempDate == parentTempDate and childTempBody == parentTempBody: continue
						if sub.find_element_by_xpath('..').find_element_by_xpath('..').get_attribute('id') == x.get_attribute('id'):
							childParentTemp = childTempUser + parentTempBody + str(childTempDate)
						else:
							childParentTemp = sub.find_element_by_xpath('..').find_element_by_xpath('..').find_element_by_xpath('.//div[contains(@class, "entry unvoted")]').find_element_by_class_name('usertext').find_element_by_xpath('.//div[1]').text
						childrenTempDict['%s-%s' % (self.mainComments.index(x), self.childrenTemp.index(sub))] = [self.removeCommonTags(childTempUser), self.removeCommonTags(childTempBody).encode('utf-8').decode('utf-8'), childTempDate, childTempScore, childParentTemp]
						'''
						Each child has a dictionary spot at the end of its item list which expresses its parent as a combined string of its parent's user, text body, and datetime so you can pull based on that to tie them together!
						'''

				except NoSuchElementException: 
					pass
				'''
				Value is None if the comment is pulled before score is revealed or element doesn't exist
				'''
				self.commentDict[self.mainComments.index(x)] = [self.removeCommonTags(parentTempUser), self.removeCommonTags(parentTempBody).encode('utf-8').decode('utf-8'), parentTempDate, parentTempScore, childrenTempDict]

		return self.commentDict

	def pullMeta(self, url='http://roosterteeth.com/archive/?id=10588&v'):

		self.metaDict = {}
		'''

		Pull likes, views, comment count, title, description, subreddit, series, publish date, etc.

		'''
		return self.metaDict

	def pullAll(self, url='http://roosterteeth.com/archive/?id=10588&v'):
		'''

		Pull all data and return both dictionaries

		'''
		commentDict = self.pullComments(url)
		metaDict = self.pullMeta(url)

		return metaDict, commentDict

def demo():
	video = mixedMeta()
	testComments = video.pullComments('http://www.reddit.com/r/photoshopbattles/comments/34ffit/psbattle_emilia_clarkes_reaction_to_kristen_wiig/')
	video.shutDown()
	print(testComments)

if __name__ == '__main__':
	
	os.system("TASKKILL /F /IM phantomjs.exe")
	demo()